import {
  ConflictException,
  Injectable,
  ValidationPipe,
  Body,
  UnauthorizedException,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from '../user/dtos/create-user.dto';
import { UserDetails } from '../user/user-details.interface';
import { ExistingUserDto } from '../user/dtos/existing-user-dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 12);
  }

  async register(
    @Body(new ValidationPipe()) user: CreateUserDto,
  ): Promise<UserDetails> {
    const { username, password } = user;

    const existingUser = await this.userService.findUserByEmail(username);

    if (existingUser) {
      throw new ConflictException(
        `User with email: ${existingUser.username} already exists`,
      );
    }
    const hashedPassword = await this.hashPassword(password);

    const newUser = await this.userService.createUser({
      ...user,
      password: hashedPassword,
    });

    return this.userService._getUserDetails(newUser);
  }

  async isPasswordMatch(
    password: string,
    hashedPassword: string,
  ): Promise<Boolean> {
    return bcrypt.compare(password, hashedPassword);
  }

  async validateUser(email: string, password: string): Promise<UserDetails> {
    const user = await this.userService.findUserByEmail(email);
    const isUserExist = !!user;

    if (!isUserExist) {
      throw new UnauthorizedException('Invalid username');
    }

    const isPasswordMatch = await this.isPasswordMatch(password, user.password);

    if (!isPasswordMatch) {
      throw new UnauthorizedException('Invalid password');
    }

    return this.userService._getUserDetails(user);
  }

  async login(
    existingUser: ExistingUserDto,
  ): Promise<{ token: string; id: string }> {
    const { username, password } = existingUser;

    const user = await this.validateUser(username, password);

    if (!user) {
      throw new UnauthorizedException('Invalid username or password');
    }

    const jwt = await this.jwtService.signAsync({ user });
    return { token: jwt, id: user.id };
  }
}
