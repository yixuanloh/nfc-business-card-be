import {
  Controller,
  Post,
  Body,
  HttpCode,
  HttpStatus,
  ConflictException,
  BadRequestException,
  ValidationPipe,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../user/dtos/create-user.dto';
import { UserDetails } from '../user/user-details.interface';
import { ExistingUserDto } from '../user/dtos/existing-user-dto';
import { JwtGuard } from './guards/jwt.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('register')
  async register(
    @Body(new ValidationPipe()) user: CreateUserDto,
  ): Promise<UserDetails> {
    try {
      return await this.authService.register(user);
    } catch (error) {
      if (error instanceof ConflictException) {
        throw new ConflictException(error.message);
      } else if (error instanceof BadRequestException) {
        throw new BadRequestException(error.message);
      } else {
        throw new Error(error.message);
      }
    }
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(
    @Body() existingUser: ExistingUserDto,
  ): Promise<{ token: string }> {
    try {
      return await this.authService.login(existingUser);
    } catch (error) {
      if (error instanceof UnauthorizedException) {
        throw new UnauthorizedException(error.message);
      } else {
        throw new Error(error.message);
      }
    }
  }
}
