import { BadRequestException, Injectable } from '@nestjs/common';
import { Bucket, Storage } from '@google-cloud/storage';

@Injectable()
export class CloudStorageService {
  private bucket: string;
  private storage: Storage;

  constructor() {
    this.storage = new Storage({
      projectId: process.env.GCP_PROJECT_ID,
      credentials: {
        client_email: process.env.GCP_CLIENT_EMAIL,
        private_key: process.env.GCP_PRIVATE_KEY.replace(/\\n/gm, '\n'),
      },
    });
    this.bucket = process.env.GCS_BUCKET_NAME;
  }

  async uploadFile(
    file: Express.Multer.File,
    path: string,
  ): Promise<{ profileImageUrl: string }> {
    const newFile = this.storage.bucket(this.bucket).file(path);
    try {
      await newFile.save(file.buffer, {
        contentType: file.mimetype,
      });
    } catch (error) {
      throw new BadRequestException(error.message);
    }
    const [url] = await newFile.getSignedUrl({
      action: 'read',
      expires: Date.now() + 10 * 365 * 24 * 60 * 60 * 1000,
    });
    return { profileImageUrl: url };
  }
}
