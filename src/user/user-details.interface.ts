export interface UserDetails {
  id: string;
  username: string;
  profileImageUrl: string;
  profileSection: {
    title: string;
    name: string;
  };
  socialMediaSection: {
    icon: string;
    url: string;
  }[];
  aboutSection: {
    title: string;
    description: string;
  };
  contactSection: {
    title: string;
    contacts: {
      title: string;
      description: string;
      type: string;
    }[];
  };
}
