import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type Contact = {
  title: string;
  description: string;
  type: 'phone' | 'email' | 'address';
};

@Schema({ _id: false })
export class ProfileSection {
  @Prop()
  title: string;
  @Prop()
  name: string;
}

@Schema({ _id: false })
export class AboutSection {
  @Prop()
  title: string;
  @Prop()
  description: string;
}

@Schema({ _id: false })
export class ContactSection {
  @Prop()
  title: string;
  @Prop()
  contacts: Contact[];
}

@Schema({ _id: false })
export class SocialMediaSection {
  @Prop()
  icon: string;
  @Prop()
  url: string;
}

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ required: true })
  username: string;

  @Prop({ required: true })
  password: string;

  @Prop()
  profileImageUrl: string;

  @Prop({ type: ProfileSection })
  profileSection: ProfileSection;

  @Prop({ type: AboutSection })
  aboutSection: AboutSection;

  @Prop({ type: ContactSection })
  contactSection: ContactSection;

  @Prop([SocialMediaSection])
  socialMediaSection: SocialMediaSection[];
}

export const UserSchema = SchemaFactory.createForClass(User);
