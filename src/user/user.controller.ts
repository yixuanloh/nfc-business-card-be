import {
  Controller,
  Get,
  Param,
  Patch,
  UseGuards,
  Body,
  Delete,
  NotFoundException,
  ValidationPipe,
  BadRequestException,
  UseInterceptors,
  Post,
  UploadedFile,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserDetails } from './user-details.interface';
import { JwtGuard } from '../auth/guards/jwt.guard';
import { UpdateUserDto } from './dtos/update-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { CloudStorageService } from 'src/cloud-storage/cloud-storage.service';

@Controller('user')
export class UserController {
  constructor(
    private userService: UserService,
    private cloudStorageService: CloudStorageService,
  ) {}

  @Get(':id')
  async getUserById(@Param('id') id: string): Promise<UserDetails> {
    try {
      const user = await this.userService.findUserById(id);
      return this.userService._getUserDetails(user);
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException(error.message);
      } else {
        throw new Error(error.message);
      }
    }
  }

  @Patch(':id')
  @UseGuards(JwtGuard)
  async updateUser(
    @Param('id') id: string,
    @Body(new ValidationPipe()) user: UpdateUserDto,
  ): Promise<UserDetails> {
    try {
      const updatedUser = await this.userService.updateUser(id, user);
      return this.userService._getUserDetails(updatedUser);
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw new BadRequestException(error.message);
      } else {
        throw new Error(error.message);
      }
    }
  }

  @Delete(':id')
  @UseGuards(JwtGuard)
  async deleteUser(@Param('id') id: string): Promise<UserDetails> {
    try {
      const deletedUser = await this.userService.deleteUser(id);
      return this.userService._getUserDetails(deletedUser);
    } catch (error) {
      if (error instanceof NotFoundException) {
        throw new NotFoundException(error.message);
      } else {
        throw new Error(error.message);
      }
    }
  }

  @Post('profileImage')
  @UseGuards(JwtGuard)
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: { userId: string },
  ): Promise<{ profileImageUrl: string }> {
    try {
      return await this.cloudStorageService.uploadFile(
        file,
        `profile-image/${body.userId}`,
      );
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }
}
