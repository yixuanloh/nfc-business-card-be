import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { User } from './schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dtos/create-user.dto';
import { UserDetails } from './user-details.interface';
import { UpdateUserDto } from './dtos/update-user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
  ) {}

  // filtered out unnecessary field like password
  _getUserDetails(user: User): UserDetails {
    return {
      id: user._id.toHexString(),
      username: user.username,
      profileImageUrl: user.profileImageUrl,
      profileSection: {
        ...user.profileSection,
      },
      socialMediaSection: [...user.socialMediaSection],
      aboutSection: {
        ...user.aboutSection,
      },
      contactSection: {
        ...user.contactSection,
      },
    };
  }

  async findUserByEmail(username: string): Promise<User> {
    const user = await this.userModel.findOne({ username }).lean().exec();
    return user;
  }

  async findUserById(id: string): Promise<User> {
    const user = await this.userModel.findById(id).lean().exec();
    if (!user) {
      throw new NotFoundException(`User with id: ${id} is not found`);
    }
    return user;
  }

  async createUser(user: CreateUserDto): Promise<User> {
    const newUser = new this.userModel(user);
    if (!newUser) {
      throw new BadRequestException('Bad Request Exception');
    }
    return newUser.save();
  }

  async updateUser(id: string, user: UpdateUserDto): Promise<User> {
    const updatedUser = await this.userModel
      .findByIdAndUpdate(id, user, {
        new: true,
      })
      .lean()
      .exec();
    if (!updatedUser) {
      throw new BadRequestException('Bad Request Exception');
    }
    return updatedUser;
  }

  async deleteUser(id: string): Promise<User> {
    const deletedUser = await this.userModel
      .findByIdAndDelete(id)
      .lean()
      .exec();
    if (!deletedUser) {
      throw new NotFoundException(`User with id: ${id} is not found`);
    }
    return deletedUser;
  }
}
