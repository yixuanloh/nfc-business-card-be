import { PartialType } from '@nestjs/mapped-types';
import {
  IsEmail,
  IsEnum,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class UpdateContactDto {
  @IsOptional()
  @IsString()
  title?: string;

  @IsOptional()
  @IsString()
  description?: string;

  @IsOptional()
  @IsEnum(['phone', 'email', 'address'])
  type?: 'phone' | 'email' | 'address';
}

export class UpdateProfileSectionDto {
  @IsOptional()
  @IsString()
  title?: string;

  @IsOptional()
  @IsString()
  name?: string;
}

export class UpdateAboutSectionDto {
  @IsOptional()
  @IsString()
  title?: string;

  @IsOptional()
  @IsString()
  description?: string;
}

export class UpdateSocialMediaSectionDto {
  @IsOptional()
  @IsString()
  icon?: string;

  @IsOptional()
  @IsString()
  url?: string;
}

export class UpdateContactSectionDto {
  @IsOptional()
  @IsString()
  title?: string;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => UpdateContactDto)
  contacts?: UpdateContactDto[];
}

export class UpdateUser {
  @IsOptional()
  @IsString()
  username?: string;

  @IsOptional()
  @IsString()
  password?: string;

  @IsOptional()
  @IsString()
  profileImageUrl?: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateProfileSectionDto)
  profileSection?: UpdateProfileSectionDto;

  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateAboutSectionDto)
  aboutSection?: UpdateAboutSectionDto;

  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateContactSectionDto)
  contactSection?: UpdateContactSectionDto;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => UpdateSocialMediaSectionDto)
  socialMediaSection?: UpdateSocialMediaSectionDto[];
}

export class UpdateUserDto extends PartialType(UpdateUser) {}
