import { IsEmail, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';

class ProfileSectionDto {
  title: string;
  name: string;
}

class AboutSectionDto {
  title: string;
  description: string;
}

class ContactDto {
  title: string;
  description: string;
  type: 'phone' | 'email' | 'address';
}

class ContactSectionDto {
  title: string;
  contacts: ContactDto[];
}

class SocialMediaSectionDto {
  icon: string;
  url: string;
}

export class ExistingUserDto {
  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  password: string;

  profileImageUrl: string;

  @Type(() => ProfileSectionDto)
  profileSection?: ProfileSectionDto;

  @Type(() => AboutSectionDto)
  aboutSection?: AboutSectionDto;

  @Type(() => ContactSectionDto)
  contactSection?: ContactSectionDto;

  @Type(() => SocialMediaSectionDto)
  socialMediaSection?: SocialMediaSectionDto[];
}
